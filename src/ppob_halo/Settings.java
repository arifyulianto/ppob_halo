package ppob_halo;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author ngonar
 */
public class Settings {

    public static String connectionString = "";
    public static String connectionDriver = "";
    public static String connectionUser = "";
    public static String connectionPass = "";

    public static String switching_ip = "";
    public static String switching_port = "";
    public static String switching_cid = "";
    public static String switcher_id = "";
    public static String bank_code = "";

    public static String pln_post_ip = "";
    public static String pln_post_port = "";
    public static String pln_post_sleep = "";

    public static String pln_pre_ip = "";
    public static String pln_pre_port = "";
    public static String pln_pre_sleep = "";

    public static String pln_non_ip = "";
    public static String pln_non_port = "";
    public static String pln_non_sleep = "";

    public static String pln_postpaid_info = "";
    private static String kodeProduk = "";

    private static String mitraToRecap = "";

    public static String rabbit_host = "";
    private static String rabbit_exchange = "";
    private static String rabbit_queueName = "";
    private static String rabbit_bindingKey = "";

    private static String rabbit_hostOut = "";
    private static String rabbit_exchangeOut = "";
    private static String rabbit_queueNameOut = "";
    private static String rabbit_bindingKeyOut = "";

    private static Map<String, String> persistenceMap = new HashMap<String, String>();

    public static int NTHREDS = 1;
    public static int chunk = 1;

    private static Logger logger = Logger.getLogger(Settings.class);

    /**
     * @return the kodeProduk
     */
    public static String getKodeProduk() {
        return kodeProduk;
    }

    /**
     * @param aKodeProduk the kodeProduk to set
     */
    public static void setKodeProduk(String aKodeProduk) {
        kodeProduk = aKodeProduk;
    }

    /**
     * @return the mitraToRecap
     */
    public static String getMitraToRecap() {
        return mitraToRecap;
    }

    /**
     * @param aMitraToRecap the mitraToRecap to set
     */
    public static void setMitraToRecap(String aMitraToRecap) {
        mitraToRecap = aMitraToRecap;
    }

    /**
     * @return the persistenceMap
     */
    public Map<String, String> getPersistenceMap() {
        return persistenceMap;
    }

    /**
     * @param aPersistenceMap the persistenceMap to set
     */
    public void setPersistenceMap(Map<String, String> aPersistenceMap) {
        persistenceMap = aPersistenceMap;
    }

    /**
     * @return the rabbit_exchange
     */
    public String getRabbit_exchange() {
        return rabbit_exchange;
    }

    /**
     * @param aRabbit_exchange the rabbit_exchange to set
     */
    public void setRabbit_exchange(String aRabbit_exchange) {
        rabbit_exchange = aRabbit_exchange;
    }

    /**
     * @return the rabbit_queueName
     */
    public String getRabbit_queueName() {
        return rabbit_queueName;
    }

    /**
     * @param aRabbit_queueName the rabbit_queueName to set
     */
    public void setRabbit_queueName(String aRabbit_queueName) {
        rabbit_queueName = aRabbit_queueName;
    }

    /**
     * @return the rabbit_bindingKey
     */
    public String getRabbit_bindingKey() {
        return rabbit_bindingKey;
    }

    /**
     * @param aRabbit_bindingKey the rabbit_bindingKey to set
     */
    public void setRabbit_bindingKey(String aRabbit_bindingKey) {
        rabbit_bindingKey = aRabbit_bindingKey;
    }

    /**
     * @return the rabbit_hostOut
     */
    public String getRabbit_hostOut() {
        return rabbit_hostOut;
    }

    /**
     * @param aRabbit_hostOut the rabbit_hostOut to set
     */
    public void setRabbit_hostOut(String aRabbit_hostOut) {
        rabbit_hostOut = aRabbit_hostOut;
    }

    /**
     * @return the rabbit_exchangeOut
     */
    public String getRabbit_exchangeOut() {
        return rabbit_exchangeOut;
    }

    /**
     * @param aRabbit_exchangeOut the rabbit_exchangeOut to set
     */
    public void setRabbit_exchangeOut(String aRabbit_exchangeOut) {
        rabbit_exchangeOut = aRabbit_exchangeOut;
    }

    /**
     * @return the rabbit_queueNameOut
     */
    public String getRabbit_queueNameOut() {
        return rabbit_queueNameOut;
    }

    /**
     * @param aRabbit_queueNameOut the rabbit_queueNameOut to set
     */
    public void setRabbit_queueNameOut(String aRabbit_queueNameOut) {
        rabbit_queueNameOut = aRabbit_queueNameOut;
    }

    /**
     * @return the rabbit_bindingKeyOut
     */
    public String getRabbit_bindingKeyOut() {
        return rabbit_bindingKeyOut;
    }

    /**
     * @param aRabbit_bindingKeyOut the rabbit_bindingKeyOut to set
     */
    public void setRabbit_bindingKeyOut(String aRabbit_bindingKeyOut) {
        rabbit_bindingKeyOut = aRabbit_bindingKeyOut;
    }

    public Settings() {

    }

    public void setPostInfo(String info) {
        this.pln_postpaid_info = info;
    }

    public String getPostInfo() {
        return pln_postpaid_info;
    }

    public void setPostIP(String url) {
        Settings.pln_post_ip = url;
    }

    public String getPostIP() {
        return Settings.pln_post_ip;
    }

    public void setPostPort(String url) {
        Settings.pln_post_port = url;
    }

    public String getPostPort() {
        return Settings.pln_post_port;
    }

    public void setPreIP(String url) {
        Settings.pln_pre_ip = url;
    }

    public String getPreIP() {
        return Settings.pln_pre_ip;
    }

    public void setPrePort(String url) {
        Settings.pln_pre_port = url;
    }

    public String getPrePort() {
        return Settings.pln_pre_port;
    }

    public void setNonIP(String url) {
        Settings.pln_non_ip = url;
    }

    public String getNonIP() {
        return Settings.pln_non_ip;
    }

    public void setNonPort(String url) {
        Settings.pln_non_port = url;
    }

    public String getNonPort() {
        return Settings.pln_non_port;
    }

    public void setNonSleep(String url) {
        Settings.pln_non_sleep = url;
    }

    public String getNonSleep() {
        return Settings.pln_non_sleep;
    }

    public void setPreSleep(String url) {
        Settings.pln_pre_sleep = url;
    }

    public String getPreSleep() {
        return Settings.pln_pre_sleep;
    }

    public void setPostSleep(String url) {
        Settings.pln_post_sleep = url;
    }

    public String getPostSleep() {
        return Settings.pln_post_sleep;
    }

    //Switching Host
    public void setSwitchingIP(String url) {
        Settings.switching_ip = url;
    }

    public String getSwitchingIP() {
        return Settings.switching_ip;
    }

    public void setSwitchingPort(String x) {
        Settings.switching_port = x;
    }

    public String getSwitchingPort() {
        return Settings.switching_port;
    }

    public void setCID(String x) {
        Settings.switching_cid = x;
    }

    public String getSwitchingCID() {
        return Settings.switching_cid;
    }

    public void setSwitcherID(String x) {
        Settings.switcher_id = x;
    }

    public String getSwitcherID() {
        return Settings.switcher_id;
    }

    public void setBankCode(String x) {
        Settings.bank_code = x;
    }

    public String getBankCode() {
        return Settings.bank_code;
    }

    //st24
    public void setRabbitHost(String url) {
        Settings.rabbit_host = url;
    }

    public String getRabbitHost() {
        return Settings.rabbit_host;
    }

    //threadpool
    public void setNthread(int x) {
        Settings.NTHREDS = x;
    }

    public int getNthread() {
        return Settings.NTHREDS;
    }

    public void setChunk(int x) {
        Settings.chunk = x;
    }

    public int getChunk() {
        return Settings.chunk;
    }

    public void setConnectionString(String con) {
        Settings.connectionString = con;
    }

    public void setConnectionDriver(String drv) {
        Settings.connectionDriver = drv;
    }

    public void setConnectionUser(String user) {
        Settings.connectionUser = user;
    }

    public void setConnectionPass(String pass) {
        Settings.connectionPass = pass;
    }

    public String getConnectionString() {
        return Settings.connectionString;
    }

    public String getConnectionDriver() {
        return Settings.connectionDriver;
    }

    public String getConnectionUser() {
        return Settings.connectionUser;
    }

    public String getConnectionPass() {
        return Settings.connectionPass;
    }

    public Connection getConnection() {
        Connection con = null;
        try {

            Class.forName(this.getConnectionDriver());
            con = DriverManager.getConnection(this.getConnectionString(), this.getConnectionUser(), this.getConnectionPass());

            System.out.println("Connection ok.");

            return con;

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e);
        }

        return con;
    }

    public void setConnections() {
        try {
            File f = new File("setting.properties");
            if (f.exists()) {
                Properties pro = new Properties();
                FileInputStream in = new FileInputStream(f);
                pro.load(in);
                //System.out.println("All key are given: " + pro.keySet());
                String conStr = pro.getProperty("Application.database.url");
                String conDrv = pro.getProperty("Application.database.driver");
                String conUser = pro.getProperty("Application.database.user");
                String conPass = pro.getProperty("Application.database.pass");
                String nthread = pro.getProperty("Application.NTHREDS");
                String nchunk = pro.getProperty("Application.chunk");

                String switching_ip = pro.getProperty("Switching.hulu.ip");
                String switching_port = pro.getProperty("Switching.hulu.port");
                String switching_cid = pro.getProperty("Switching.hulu.cid");
                String switching_swid = pro.getProperty("Switching.hulu.switcher_id");
                String switching_bank = pro.getProperty("Switching.hulu.bank_code");

                String pln_post_ip = pro.getProperty("pln.post.ip");
                String pln_post_port = pro.getProperty("pln.post.port");
                String pln_post_sleep = pro.getProperty("pln.post.sleep");
                String pln_post_info = pro.getProperty("pln.post.info");

                String pln_pre_ip = pro.getProperty("pln.pre.ip");
                String pln_pre_port = pro.getProperty("pln.pre.port");
                String pln_pre_sleep = pro.getProperty("pln.pre.sleep");

                String pln_non_ip = pro.getProperty("pln.non.ip");
                String pln_non_port = pro.getProperty("pln.non.port");
                String pln_non_sleep = pro.getProperty("pln.non.sleep");

                String kodeProduk = pro.getProperty("bpjs.kes.ppob.id");
                String mitraToRecap = pro.getProperty("bpjs.kes.ppob.recap");
//asdasdf
                setMitraToRecap(mitraToRecap);
                setKodeProduk(kodeProduk);
                setConnectionString(conStr);
                setConnectionDriver(conDrv);
                setConnectionUser(conUser);
                setConnectionPass(conPass);
                setNthread(Integer.parseInt(nthread));
                setChunk(Integer.parseInt(nchunk));
                setCID(switching_cid);
                setSwitchingIP(switching_ip);
                setSwitchingPort(switching_port);
                setSwitcherID(switching_swid);
                setBankCode(switching_bank);
                setPostIP(pln_post_ip);
                setPostPort(pln_post_port);
                setPostSleep(pln_post_sleep);
                setPostInfo(pln_post_info);
                setPreIP(pln_pre_ip);
                setPrePort(pln_pre_port);
                setPreSleep(pln_pre_sleep);
                setNonIP(pln_non_ip);
                setNonPort(pln_non_port);
                setNonSleep(pln_non_sleep);

                //st24
                String mq = pro.getProperty("Pelangi.rabbit.host");
                setRabbitHost(mq);

                String rabbitHost = pro.getProperty("Pelangi.rabbit.host.in");
                String rabbitExchange = pro.getProperty("Pelangi.rabbit.exchange.in");
                String rabbitQueueName = pro.getProperty("Pelangi.rabbit.queueName.in");
                String rabbitBindingKey = pro.getProperty("Pelangi.rabbit.bindingKey.in");

                String rabbitHostOut = pro.getProperty("Pelangi.rabbit.host.out");
                String rabbitExchangeOut = pro.getProperty("Pelangi.rabbit.exchange.out");
                String rabbitQueueNameOut = pro.getProperty("Pelangi.rabbit.queueName.out");
                String rabbitBindingKeyOut = pro.getProperty("Pelangi.rabbit.bindingKey.out");

                setRabbit_hostOut(rabbitHostOut);
                setRabbit_bindingKeyOut(rabbitBindingKeyOut);
                setRabbit_exchangeOut(rabbitExchangeOut);
                setRabbit_queueNameOut(rabbitQueueNameOut);

                setRabbitHost(rabbitHost);
                setRabbit_bindingKey(rabbitBindingKey);
                setRabbit_exchange(rabbitExchange);
                setRabbit_queueName(rabbitQueueName);

                Map<String, String> persistenceMap = new HashMap<String, String>();
                persistenceMap.put("openjpa.ConnectionURL", pro.getProperty("persistence.url"));
                persistenceMap.put("openjpa.ConnectionUserName", pro.getProperty("persistence.user"));
                persistenceMap.put("openjpa.ConnectionPassword", pro.getProperty("persistence.pass"));
                persistenceMap.put("openjpa.ConnectionDriverName", pro.getProperty("persistence.driver"));
                setPersistenceMap(persistenceMap);

            } else {
                System.out.println("File setting not found");
            }
        } catch (Exception e) {
            logger.log(Level.FATAL, e);
        }
    }

}
