/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author root
 */
@Entity
@Table(name = "token_unsolds")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TokenUnsolds.findAll", query = "SELECT t FROM TokenUnsolds t"),
    @NamedQuery(name = "TokenUnsolds.findById", query = "SELECT t FROM TokenUnsolds t WHERE t.id = :id"),
    @NamedQuery(name = "TokenUnsolds.findBySnmeter", query = "SELECT t FROM TokenUnsolds t WHERE t.snmeter = :snmeter"),
    @NamedQuery(name = "TokenUnsolds.findByIdpel", query = "SELECT t FROM TokenUnsolds t WHERE t.idpel = :idpel"),
    @NamedQuery(name = "TokenUnsolds.findBySold", query = "SELECT t FROM TokenUnsolds t WHERE t.sold = :sold"),
    @NamedQuery(name = "TokenUnsolds.findBySoldDate", query = "SELECT t FROM TokenUnsolds t WHERE t.soldDate = :soldDate"),
    @NamedQuery(name = "TokenUnsolds.findByCreateDate", query = "SELECT t FROM TokenUnsolds t WHERE t.createDate = :createDate"),
    @NamedQuery(name = "TokenUnsolds.findByCreateBy", query = "SELECT t FROM TokenUnsolds t WHERE t.createBy = :createBy"),
    @NamedQuery(name = "TokenUnsolds.findByNominal", query = "SELECT t FROM TokenUnsolds t WHERE t.nominal = :nominal"),
    @NamedQuery(name = "TokenUnsolds.findByIso", query = "SELECT t FROM TokenUnsolds t WHERE t.iso = :iso"),
    @NamedQuery(name = "TokenUnsolds.findByInboxId", query = "SELECT t FROM TokenUnsolds t WHERE t.inboxId = :inboxId")})
public class TokenUnsolds implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 15)
    @Column(name = "snmeter")
    private String snmeter;
    @Size(max = 15)
    @Column(name = "idpel")
    private String idpel;
    @Column(name = "sold")
    private Boolean sold;
    @Column(name = "sold_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date soldDate;
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Size(max = 20)
    @Column(name = "create_by")
    private String createBy;
    @Column(name = "nominal")
    private Integer nominal;
    @Size(max = 500)
    @Column(name = "iso")
    private String iso;
    @Column(name = "inbox_id")
    private Integer inboxId;

    public TokenUnsolds() {
    }

    public TokenUnsolds(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSnmeter() {
        return snmeter;
    }

    public void setSnmeter(String snmeter) {
        this.snmeter = snmeter;
    }

    public String getIdpel() {
        return idpel;
    }

    public void setIdpel(String idpel) {
        this.idpel = idpel;
    }

    public Boolean getSold() {
        return sold;
    }

    public void setSold(Boolean sold) {
        this.sold = sold;
    }

    public Date getSoldDate() {
        return soldDate;
    }

    public void setSoldDate(Date soldDate) {
        this.soldDate = soldDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Integer getNominal() {
        return nominal;
    }

    public void setNominal(Integer nominal) {
        this.nominal = nominal;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public Integer getInboxId() {
        return inboxId;
    }

    public void setInboxId(Integer inboxId) {
        this.inboxId = inboxId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TokenUnsolds)) {
            return false;
        }
        TokenUnsolds other = (TokenUnsolds) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.TokenUnsolds[ id=" + id + " ]";
    }
    
}
