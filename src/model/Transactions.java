/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ngonar
 */
@Entity
@Table(name = "transactions")
@NamedQueries({
    @NamedQuery(name = "Transactions.findAll", query = "SELECT t FROM Transactions t"),
    @NamedQuery(name = "Transactions.findById", query = "SELECT t FROM Transactions t WHERE t.id = :id"),
    @NamedQuery(name = "Transactions.findByUserId", query = "SELECT t FROM Transactions t WHERE t.userId = :userId"),
    @NamedQuery(name = "Transactions.findByProductId", query = "SELECT t FROM Transactions t WHERE t.productId = :productId"),
    @NamedQuery(name = "Transactions.findByDestination", query = "SELECT t FROM Transactions t WHERE t.destination = :destination"),
    @NamedQuery(name = "Transactions.findBySender", query = "SELECT t FROM Transactions t WHERE t.sender = :sender"),
    @NamedQuery(name = "Transactions.findBySenderType", query = "SELECT t FROM Transactions t WHERE t.senderType = :senderType"),
    @NamedQuery(name = "Transactions.findByCreateDate", query = "SELECT t FROM Transactions t WHERE t.createDate = :createDate"),
    @NamedQuery(name = "Transactions.findByPriceSell", query = "SELECT t FROM Transactions t WHERE t.priceSell = :priceSell"),
    @NamedQuery(name = "Transactions.findByPriceBuy", query = "SELECT t FROM Transactions t WHERE t.priceBuy = :priceBuy"),
    @NamedQuery(name = "Transactions.findByInboxId", query = "SELECT t FROM Transactions t WHERE t.inboxId = :inboxId"),
    @NamedQuery(name = "Transactions.findByStatus", query = "SELECT t FROM Transactions t WHERE t.status = :status"),
    @NamedQuery(name = "Transactions.findByTerminalId", query = "SELECT t FROM Transactions t WHERE t.terminalId = :terminalId"),
    @NamedQuery(name = "Transactions.findByRemark", query = "SELECT t FROM Transactions t WHERE t.remark = :remark"),
    @NamedQuery(name = "Transactions.findByFormatBalasanId", query = "SELECT t FROM Transactions t WHERE t.formatBalasanId = :formatBalasanId"),
    @NamedQuery(name = "Transactions.findBySaldoAwal", query = "SELECT t FROM Transactions t WHERE t.saldoAwal = :saldoAwal"),
    @NamedQuery(name = "Transactions.findByPerintah", query = "SELECT t FROM Transactions t WHERE t.perintah = :perintah"),
    @NamedQuery(name = "Transactions.findByPrioritas", query = "SELECT t FROM Transactions t WHERE t.prioritas = :prioritas"),
    @NamedQuery(name = "Transactions.findByCounter", query = "SELECT t FROM Transactions t WHERE t.counter = :counter"),
    @NamedQuery(name = "Transactions.findByCounter2", query = "SELECT t FROM Transactions t WHERE t.counter2 = :counter2"),
    @NamedQuery(name = "Transactions.findBySn", query = "SELECT t FROM Transactions t WHERE t.sn = :sn"),
    @NamedQuery(name = "Transactions.findByReceiver", query = "SELECT t FROM Transactions t WHERE t.receiver = :receiver"),
    @NamedQuery(name = "Transactions.findByResend", query = "SELECT t FROM Transactions t WHERE t.resend = :resend")})

@SequenceGenerator(sequenceName="transactions_id_seq",name="trx_gen", allocationSize=1)
public class Transactions implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trx_gen")
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "product_id")
    private Integer productId;
    @Column(name = "destination")
    private String destination;
    @Column(name = "sender")
    private String sender;
    @Column(name = "sender_type")
    private String senderType;
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "price_sell")
    private Long priceSell;
    @Column(name = "price_buy")
    private Long priceBuy;
    @Column(name = "inbox_id")
    private Integer inboxId;
    @Column(name = "status")
    private String status;
    @Column(name = "terminal_id")
    private Integer terminalId;
    @Column(name = "remark")
    private String remark;
    @Column(name = "format_balasan_id")
    private Integer formatBalasanId;
    @Column(name = "saldo_awal")
    private Long saldoAwal;
    @Column(name = "perintah")
    private String perintah;
    @Column(name = "prioritas")
    private Short prioritas;
    @Column(name = "counter")
    private Short counter;
    @Column(name = "counter2")
    private Short counter2;
    @Column(name = "sn")
    private String sn;
    @Column(name = "receiver")
    private String receiver;
    @Column(name = "resend")
    private Short resend;

    public Transactions() {
    }

    public Transactions(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSenderType() {
        return senderType;
    }

    public void setSenderType(String senderType) {
        this.senderType = senderType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getPriceSell() {
        return priceSell;
    }

    public void setPriceSell(Long priceSell) {
        this.priceSell = priceSell;
    }

    public Long getPriceBuy() {
        return priceBuy;
    }

    public void setPriceBuy(Long priceBuy) {
        this.priceBuy = priceBuy;
    }

    public Integer getInboxId() {
        return inboxId;
    }

    public void setInboxId(Integer inboxId) {
        this.inboxId = inboxId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Integer terminalId) {
        this.terminalId = terminalId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getFormatBalasanId() {
        return formatBalasanId;
    }

    public void setFormatBalasanId(Integer formatBalasanId) {
        this.formatBalasanId = formatBalasanId;
    }

    public Long getSaldoAwal() {
        return saldoAwal;
    }

    public void setSaldoAwal(Long saldoAwal) {
        this.saldoAwal = saldoAwal;
    }

    public String getPerintah() {
        return perintah;
    }

    public void setPerintah(String perintah) {
        this.perintah = perintah;
    }

    public Short getPrioritas() {
        return prioritas;
    }

    public void setPrioritas(Short prioritas) {
        this.prioritas = prioritas;
    }

    public Short getCounter() {
        return counter;
    }

    public void setCounter(Short counter) {
        this.counter = counter;
    }

    public Short getCounter2() {
        return counter2;
    }

    public void setCounter2(Short counter2) {
        this.counter2 = counter2;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public Short getResend() {
        return resend;
    }

    public void setResend(Short resend) {
        this.resend = resend;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transactions)) {
            return false;
        }
        Transactions other = (Transactions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Transactions[id=" + id + "]";
    }

}
