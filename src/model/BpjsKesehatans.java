/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author root
 */
@Entity
@Table(name = "bpjs_kesehatans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BpjsKesehatans.findAll", query = "SELECT p FROM BpjsKesehatan p"),
    @NamedQuery(name = "BpjsKesehatans.findById", query = "SELECT p FROM BpjsKesehatan p WHERE p.id = :id"),
    @NamedQuery(name = "BpjsKesehatans.findByInboxId", query = "SELECT p FROM BpjsKesehatan p WHERE p.inboxId = :inboxId"),
    @NamedQuery(name = "BpjsKesehatans.findByTransid", query = "SELECT p FROM BpjsKesehatan p WHERE p.transid = :transid"),
    @NamedQuery(name = "BpjsKesehatans.findByPartnertid", query = "SELECT p FROM BpjsKesehatan p WHERE p.partnertid = :partnertid"),
    @NamedQuery(name = "BpjsKesehatans.findByRestype", query = "SELECT p FROM BpjsKesehatan p WHERE p.restype = :restype"),
    @NamedQuery(name = "BpjsKesehatans.findByRescode", query = "SELECT p FROM BpjsKesehatan p WHERE p.rescode = :rescode"),
    @NamedQuery(name = "BpjsKesehatans.findByIdpel", query = "SELECT p FROM BpjsKesehatan p WHERE p.idpel = :idpel"),
    @NamedQuery(name = "BpjsKesehatans.findByNama", query = "SELECT p FROM BpjsKesehatan p WHERE p.nama = :nama"),
    @NamedQuery(name = "BpjsKesehatans.findByJumlahrek", query = "SELECT p FROM BpjsKesehatan p WHERE p.jumlahrek = :jumlahrek"),
    @NamedQuery(name = "BpjsKesehatans.findByAdmin", query = "SELECT p FROM BpjsKesehatan p WHERE p.admin = :admin"),
    @NamedQuery(name = "BpjsKesehatans.findByRefnbr", query = "SELECT p FROM BpjsKesehatan p WHERE p.refnbr = :refnbr"),
    @NamedQuery(name = "BpjsKesehatans.findByInfo", query = "SELECT p FROM BpjsKesehatan p WHERE p.info = :info"),
    @NamedQuery(name = "BpjsKesehatans.findBySaldo", query = "SELECT p FROM BpjsKesehatan p WHERE p.saldo = :saldo"),
    @NamedQuery(name = "BpjsKesehatans.findByCreateDate", query = "SELECT p FROM BpjsKesehatan p WHERE p.createDate = :createDate"),
    @NamedQuery(name = "BpjsKesehatans.findByUser", query = "SELECT p FROM BpjsKesehatan p WHERE p.user = :user"),
    @NamedQuery(name = "BpjsKesehatans.findByPass", query = "SELECT p FROM BpjsKesehatan p WHERE p.pass = :pass"),
    @NamedQuery(name = "BpjsKesehatans.findByXml", query = "SELECT p FROM BpjsKesehatan p WHERE p.xml = :xml"),
    @NamedQuery(name = "BpjsKesehatans.findByIso", query = "SELECT p FROM BpjsKesehatan p WHERE p.iso = :iso"),
    @NamedQuery(name = "BpjsKesehatans.findByCid", query = "SELECT p FROM BpjsKesehatan p WHERE p.cid = :cid"),
    @NamedQuery(name = "BpjsKesehatans.findByMerchant", query = "SELECT p FROM BpjsKesehatan p WHERE p.merchant = :merchant")
})

@SequenceGenerator(sequenceName = "bpjs_kesehatans_id_seq", name = "bpjskesehatan_gen", allocationSize = 1)
public class BpjsKesehatans implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bpjskesehatan_gen")
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "inbox_id")
    private Integer inboxId;

    @Size(max = 50)
    @Column(name = "transid")
    private String transid;

    @Size(max = 50)
    @Column(name = "partnertid")
    private String partnertid;

    @Size(max = 50)
    @Column(name = "restype")
    private String restype;

    @Size(max = 50)
    @Column(name = "rescode")
    private String rescode;

    @Size(max = 50)
    @Column(name = "idpel")
    private String idpel;

    @Size(max = 255)
    @Column(name = "nama")
    private String nama;

    @Size(max = 255)
    @Column(name = "kodecabang")
    private String kodecabang;

    @Size(max = 255)
    @Column(name = "namacabang")
    private String namacabang;

    @Size(max = 50)
    @Column(name = "biayapremi")
    private String biayapremi;

    @Size(max = 50)
    @Column(name = "rptotal")
    private String rptotal;

    @Size(max = 50)
    @Column(name = "sisa")
    private String sisa;

    @Size(max = 50)
    @Column(name = "tgllunas")
    private String tgllunas;

    @Size(max = 5)
    @Column(name = "jumlahrek")
    private String jumlahrek;

    @Size(max = 50)
    @Column(name = "admin")
    private String admin;

    @Size(max = 50)
    @Column(name = "refnbr")
    private String refnbr;
    
    @Size(max = 255)
    @Column(name = "info")
    private String info;
    
    @Size(max = 50)
    @Column(name = "saldo")
    private String saldo;
    
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
//    
//    @Size(max = 255)
//    @Column(name = "destination")
//    private String destination;
    
    @Size(max = 50)
    @Column(name = "user_")
    private String user;
    
    @Size(max = 50)
    @Column(name = "pass_")
    private String pass;
    
    @Size(max = 2147483647)
    @Column(name = "xml")
    private String xml;
    
    @Size(max = 2147483647)
    @Column(name = "iso")
    private String iso;
    
    @Size(max = 50)
    @Column(name = "cid")
    private String cid;
    
    @Column(name = "reprint")
    private int reprint;

    @Size(max = 50)
    @Column(name = "merchant")
    private String merchant;

    public BpjsKesehatans() {
    }

    public BpjsKesehatans(Long id) {
        this.id = id;
    }

    public int getReprint() {
        return reprint;
    }

    public void setReprint(int transid) {
        this.reprint = transid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getInboxId() {
        return inboxId;
    }

    public void setInboxId(Integer inboxId) {
        this.inboxId = inboxId;
    }

    public String getTransid() {
        return transid;
    }

    public void setTransid(String transid) {
        this.transid = transid;
    }

    public String getPartnertid() {
        return partnertid;
    }

    public void setPartnertid(String partnertid) {
        this.partnertid = partnertid;
    }

    public String getRestype() {
        return restype;
    }

    public void setRestype(String restype) {
        this.restype = restype;
    }

    public String getRescode() {
        return rescode;
    }

    public void setRescode(String rescode) {
        this.rescode = rescode;
    }

    public String getIdpel() {
        return idpel;
    }

    public void setIdpel(String idpel) {
        this.idpel = idpel;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJumlahrek() {
        return jumlahrek;
    }

    public void setJumlahrek(String jumlahrek) {
        this.jumlahrek = jumlahrek;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getRefnbr() {
        return refnbr;
    }

    public void setRefnbr(String refnbr) {
        this.refnbr = refnbr;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

//    public String getDestination() {
//        return destination;
//    }
//
//    public void setDestination(String destination) {
//        this.destination = destination;
//    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

//    public String getTelpon() {
//        return telpon;
//    }
//
//    public void setTelpon(String telpon) {
//        this.telpon = telpon;
//    }
//
    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BpjsKesehatans)) {
            return false;
        }
        BpjsKesehatans other = (BpjsKesehatans) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.BpjsKesehatan[ id=" + id + " ]";
    }

    /**
     * @return the kodecabang
     */
    public String getKodecabang() {
        return kodecabang;
    }

    /**
     * @param kodecabang the kodecabang to set
     */
    public void setKodecabang(String kodecabang) {
        this.kodecabang = kodecabang;
    }

    /**
     * @return the namacabang
     */
    public String getNamacabang() {
        return namacabang;
    }

    /**
     * @param namacabang the namacabang to set
     */
    public void setNamacabang(String namacabang) {
        this.namacabang = namacabang;
    }

    /**
     * @return the biayapremi
     */
    public String getBiayapremi() {
        return biayapremi;
    }

    /**
     * @param biayapremi the biayapremi to set
     */
    public void setBiayapremi(String biayapremi) {
        this.biayapremi = biayapremi;
    }

    /**
     * @return the sisa
     */
    public String getSisa() {
        return sisa;
    }

    /**
     * @param sisa the sisa to set
     */
    public void setSisa(String sisa) {
        this.sisa = sisa;
    }

    /**
     * @return the tgllunas
     */
    public String getTgllunas() {
        return tgllunas;
    }

    /**
     * @param tgllunas the tgllunas to set
     */
    public void setTgllunas(String tgllunas) {
        this.tgllunas = tgllunas;
    }

    /**
     * @return the rptotal
     */
    public String getRptotal() {
        return rptotal;
    }

    /**
     * @param rptotal the rptotal to set
     */
    public void setRptotal(String rptotal) {
        this.rptotal = rptotal;
    }

}
