/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author VOLD
 */
@Entity
@Table(name = "halos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Halos.findAll", query = "SELECT h FROM Halos h")
    , @NamedQuery(name = "Halos.findById", query = "SELECT h FROM Halos h WHERE h.id = :id")
    , @NamedQuery(name = "Halos.findByIdpelanggan", query = "SELECT h FROM Halos h WHERE h.idpelanggan = :idpelanggan")
    , @NamedQuery(name = "Halos.findByNamapelanggan", query = "SELECT h FROM Halos h WHERE h.namapelanggan = :namapelanggan")
    , @NamedQuery(name = "Halos.findByNoreference", query = "SELECT h FROM Halos h WHERE h.noreference = :noreference")
    , @NamedQuery(name = "Halos.findByAmount", query = "SELECT h FROM Halos h WHERE h.amount = :amount")
    , @NamedQuery(name = "Halos.findByAdminCharge", query = "SELECT h FROM Halos h WHERE h.adminCharge = :adminCharge")
    , @NamedQuery(name = "Halos.findByIso", query = "SELECT h FROM Halos h WHERE h.iso = :iso")
    , @NamedQuery(name = "Halos.findByInboxId", query = "SELECT h FROM Halos h WHERE h.inboxId = :inboxId")})
@SequenceGenerator(sequenceName="halos_id_seq",name="halo_gen")
public class Halos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "halo_gen")
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 50)
    @Column(name = "idpelanggan")
    private String idpelanggan;
    @Size(max = 45)
    @Column(name = "namapelanggan")
    private String namapelanggan;
    @Size(max = 32)
    @Column(name = "noreference")
    private String noreference;
    @Column(name = "amount")
    private Integer amount;
    @Column(name = "admin_charge")
    private Integer adminCharge;
    @Size(max = 2147483647)
    @Column(name = "iso")
    private String iso;
    @Column(name = "inbox_id")
    private BigInteger inboxId;

    public Halos() {
    }

    public Halos(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdpelanggan() {
        return idpelanggan;
    }

    public void setIdpelanggan(String idpelanggan) {
        this.idpelanggan = idpelanggan;
    }

    public String getNamapelanggan() {
        return namapelanggan;
    }

    public void setNamapelanggan(String namapelanggan) {
        this.namapelanggan = namapelanggan;
    }

    public String getNoreference() {
        return noreference;
    }

    public void setNoreference(String noreference) {
        this.noreference = noreference;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getAdminCharge() {
        return adminCharge;
    }

    public void setAdminCharge(Integer adminCharge) {
        this.adminCharge = adminCharge;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public BigInteger getInboxId() {
        return inboxId;
    }

    public void setInboxId(BigInteger inboxId) {
        this.inboxId = inboxId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Halos)) {
            return false;
        }
        Halos other = (Halos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Halos[ id=" + id + " ]";
    }
    
}