package BILLER.SW;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author ngonar
 */
public class GenericResponseCode {

    public GenericResponseCode() {

    }

//    public String getPurchaseMsgResponseName(String code) {
    public String getResponseDesctiprion(String code) {
        if (code.equalsIgnoreCase("00")) {
            return "SUKSES";
        } else if (code.equalsIgnoreCase("03")) {
            return "Invalid Merchant Type";
        } else if (code.equalsIgnoreCase("06")) {
            return "Unknown error";
        } else if (code.equalsIgnoreCase("07")) {
            return "Transaksi gagal, Silahkan ulangi lagi";
        } else if (code.equalsIgnoreCase("09")) {
            return "Request in progress";
        } else if (code.equalsIgnoreCase("12")) {
            return "Invalid transaction";
        } else if (code.equalsIgnoreCase("13")) {
            return "Application Server is down";
        } else if (code.equalsIgnoreCase("14")) {
//            return "Unidentified/invalid Phone/ID number";
            return "No Pelanggan salah, silahkan teliti kembali";
        } else if (code.equalsIgnoreCase("18")) {
//            return "Request timeout";
            return "Maaf kemungkinan ada gangguan jaringan sehingga pelunasan no. rek norek_val gagal.\n"
                    + "silahkan transaksi ulang kembali";
//            return "Maaf kemungkinan ada gangguan jaringan sehingga pelunasan gagal.\n"
//                    + "silahkan transaksi ulang kembali";
        } else if (code.equalsIgnoreCase("21")) {
            return "Unknown MTI / Jenis pesan tidak disupport oleh biller.";
        } else if (code.equalsIgnoreCase("22")) {
            return "Unpaid transaction, jika pesan reversal dikirimkan tanpa pelunasan sebelumnya.";
        } else if (code.equalsIgnoreCase("23")) {
            return "Seat already taken / not available.";
        } else if (code.equalsIgnoreCase("24")) {
            return "Cant cancel payment, ticket has been printed";
        } else if (code.equalsIgnoreCase("25")) {
            return "Cant update pax, ticket has been paid";
        } else if (code.equalsIgnoreCase("26")) {
            return "Data penumpang / pembeli tidak lengkap";
        } else if (code.equalsIgnoreCase("27")) {
            return "Jadwal tidak tersedia.";
        } else if (code.equalsIgnoreCase("30")) {
            return "ISO packet composition failure";
        } else if (code.equalsIgnoreCase("31")) {
            return "ID bank belum terdaftar";
        } else if (code.equalsIgnoreCase("32")) {
            return "ID Switcher belum terdaftar";
        } else if (code.equalsIgnoreCase("33")) {
            return "Kode biller/produk belum terdaftar";
        } else if (code.equalsIgnoreCase("34")) {
//            return "Transaksi sudah lunas";
            return "Tagihan sudah terbayar";
        } else if (code.equalsIgnoreCase("43")) {
            return "blocked/expired phone number";
        } else if (code.equalsIgnoreCase("44")) {
            return "Voucher out of stock";
        } else if (code.equalsIgnoreCase("58")) {
            return "Transaction not permitted to terminal";
        } else if (code.equalsIgnoreCase("61")) {
            return "Deposit tidak mencukupi/invalid";
        } else if (code.equalsIgnoreCase("68")) {
//            return "Unidentified/invalid Phone/ID number";
            return "No Pelanggan salah, silahkan teliti kembali";
        } else if (code.equalsIgnoreCase("73")) {
            return "Unable to route transaction";
        } else if (code.equalsIgnoreCase("90")) {
            return "Cut-off is in process";
        } else if (code.equalsIgnoreCase("88")) {
            return "Too close to previous transactions";
        } else if (code.equalsIgnoreCase("96")) {
            return "System malfunction";
        } else if (code.equalsIgnoreCase("98")) {
            return "Invalid reference number";
        }

        return "";
    }

    public String getResponseDesctiprionBpjs(String code) {
//        PrepaidResponseCode prerc = new PrepaidResponseCode();
//        prerc.responseParser(code);

        String hasil = responseParser(code);
        return hasil;
    }
    
    public String responseParser(String code) {
        String keluaran = "";
//        System.out.println("code => "+code+" ==> "+isi);
        System.out.println("code => "+code);
        String hasil = getRc("kes", code);

//        if (code.equalsIgnoreCase("0009")
//                || code.equalsIgnoreCase("0077")
//                || code.equalsIgnoreCase("0016")) {
//            hasil = hasil.replace("[ISI]", isi);
//        }
        System.out.println("hasil");
        keluaran = hasil;
        return keluaran;
    }

    public String getRc(String layanan, String rcNumber) {
        Properties pro = null;
        FileInputStream in = null;
        String rcDescription = "";
        try {
            File f = new File("rc.properties");
            if (f.exists()) {
                pro = new Properties();
                in = new FileInputStream(f);
                pro.load(in);
                rcDescription = pro.getProperty("bpjs."+layanan+"." + rcNumber);
            } else {
//                logger.info("File rc.properties not found");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ex) {
//                    logger.log(Priority.FATAL, ex);
                    //java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }
            }
        }
        return rcDescription;
    }

}
